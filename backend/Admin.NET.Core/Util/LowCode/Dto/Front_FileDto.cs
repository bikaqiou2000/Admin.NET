﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core.Util.LowCode.Dto
{
    public class Front_FileDto
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Uid { get; set; }
        public string Url { get; set; }
    }
}
