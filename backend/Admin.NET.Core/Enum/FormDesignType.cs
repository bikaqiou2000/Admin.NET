﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Furion.Extras.Admin.NET
{
    /// <summary>
    /// 动态表单类型
    /// </summary>
    public enum FormDesignType
    {
        /// <summary>
        /// vue-form-design
        /// </summary>
        [Description("vue-form-design")]
        VueFormDesign = 1
    }
}
